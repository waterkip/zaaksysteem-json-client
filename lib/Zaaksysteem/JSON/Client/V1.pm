package Zaaksysteem::JSON::Client::V1;
use Moose;
use namespace::autoclean;

use JSON;
use BTTW::Tools;

extends 'Zaaksysteem::JSON::Client';

has _expected_keys => (
    is      => 'ro',
    isa     => 'ArrayRef',
    default => sub {
        return [qw(status_code api_version result request_id development)];
    },
);

has _response => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { return {} },
);

sub get {
    my ($self, $path, %query) = @_;
    return $self->_parse_api_response(
        $self->ua->get($self->_generate_uri($path, %query)));
}

sub _parse_api_response {
    my $self = shift;
    my ($response) = @_;

    if (!$response->is_success) {
        die(sprintf("Unable to parse response for URI %s: %s\n", $response->request->uri, $response->status_line));
    }

    if ($response->content_type ne 'application/json') {
        print $response->content if $self->verbose;
        die("Not a JSON response!\n");
    }

    my $json = $response->content;
    print STDERR $json if $self->verbose;
    $json = decode_json($json);

    if (keys %$json != @{ $self->_expected_keys }) {
        die("Invalid JSON response, expected keys (v1) are not available\n");
    }
    $self->_response($json);
    return $json;
}

sub has_next {
    my $self = shift;
    if ($self->_response->{result}{instance}{pager}{next}) {
        return 1;
    }
    return 0;
}

sub has_prev {
    my $self = shift;
    if ($self->_response->{result}{instance}{pager}{prev}) {
        return 1;
    }
    return 0;
}

sub has_pages {
    my $self = shift;
    if ($self->_response->{result}{instance}{pager}{pages}) {
        return $self->_response->{result}{instance}{pager}{pages};
    }
}

sub current {
    my $self = shift;
    if ($self->_response->{result}{instance}{pager}{page}) {
        return $self->_response->{result}{instance}{pager}{page};
    }
}

sub next {
    my $self = shift;
    return undef if !$self->has_next;
    return $self->get($self->_response->{result}{instance}{pager}{next});
}

sub prev {
    my $self = shift;
    return undef if !$self->has_prev;
    return $self->get($self->_response->{result}{instance}{pager}{prev});
}

sub post {
    my ($self, $path, $data, %query) = @_;

    my $content_type = 'application/json';

    return $self->_parse_api_response(
        $self->ua->post(
            $self->_generate_uri($path, %query),
            Content_Type => $content_type,
            Content      => $data,
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::JSON::Client::V1 - A JSON client for Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::JSON::Client::V1;

    my $zs = Zaaksysteem::JSON::Client::V1->new(
        user     => 'someuser',
        password => 'password',
        url      => 'https://10.44.0.11',
        ua       => LWP::UserAgent->new(), #optional
    );

    my ($object) = $zs->post('api/object/save', { stuff => 'here'});
    my @objects  = $zs->search('api/object/search', 'SELECT {} FROM domain');
    my @objects  = $zs->get('api/object/search?zql=SELECT {} FROM domain');

=head1 ATTRIBUTES

=head2 user

The username used to login

=head2 password

The password to login

=head2 url

The base url for all the calls.

=head2 ua

An LWP::UserAgent object, we have a default you may use.

=head1 METHODS

=head2 search

Convenience method for get("PATH?zql=ZQL");

=head3 ARGUMENTS

=over

=item * path

=item * zql

=back

=head3 RETURNS

=head2 get

=over

=item * path

=back

=head2 post

=over

=item * path


=item * data

=back

=head2 login

Login to zaaksysteem

=head2 current

=head2 has_next

=head2 has_pages

=head2 has_prev

=head2 next

=head2 prev

=head1 BUGS

=head1 AUTHOR

    Wesley Schwengle
    CPAN ID: WATERKIP
    Mintlab B.V. http://www.mintlab.nl
    wesley@mintlab.nl

=head1 COPYRIGHT

Copyright 2014, Mintlab B.V. All rights reserved.

=head1 SEE ALSO

perl(1).

=cut
